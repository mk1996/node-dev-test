import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { ReportModule } from './../src/Report/ReportModule';

describe('', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [ReportModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(404);
  });

  it('Should returns best sellers in 2019-08-08', () => {
    return request(app.getHttpServer())
      .get('/report/products/2019-08-08')
      .expect(200)
      .expect([
        {
            "quantity": 1,
            "totalPrice": 110,
            "productName": "Black sport shoes"
        },
        {
            "quantity": 1,
            "totalPrice": 25.75,
            "productName": "Cotton t-shirt XL"
        },
        {
            "quantity": 1,
            "totalPrice": 55.99,
            "productName": "Blue jeans"
        }
    ]);
  });

  it('Should return empty boards if there are no orders on that day', () => {
    return request(app.getHttpServer())
      .get('/report/products/YYYY-MM-DD')
      .expect(400)
      .expect({
        statusCode: 400,
        error: 'Bad Request',
        message: 'You must enter a date in the format YYYY-MM-DD',
      });
  });

  it('Should return bad request if date is not valid', () => {
    return request(app.getHttpServer())
      .get('/report/products/2019-05-08')
      .expect(200)
      .expect([]);
  });

  it('Should returns best buyers in 2019-08-08', () => {
    return request(app.getHttpServer())
      .get('/report/customer/2019-08-08')
      .expect(200)
      .expect([
        {
            "customerName": "Jane Doe",
            "totalPrice": 110
        },
        {
            "customerName": "John Doe",
            "totalPrice": 81.74
        }
    ]);
  });

  it('Should return bad request if date is not valid', () => {
    return request(app.getHttpServer())
      .get('/report/customer/2019-05-08')
      .expect(200)
      .expect([]);
  });

  it('Should return bad request if date is not valid', () => {
    return request(app.getHttpServer())
      .get('/report/customer/YYYY-MM-DD')
      .expect(400)
      .expect({
        statusCode: 400,
        error: 'Bad Request',
        message: 'You must enter a date in the format YYYY-MM-DD',
      });
  });
});
