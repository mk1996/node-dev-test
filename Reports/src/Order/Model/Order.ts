import { Customer } from "./Customer";
import { Product } from "./Product";

export class Order {
    number: string;
    customer: Customer;
    createdAt: string;
    products: Product[];

    get totalPrice(): number {
      return this.products.map(product => product.price).reduce((a, b) => a + b, 0);
    }
}
