import { Test } from '@nestjs/testing';
import { OrderMapper } from './OrderMapper';
import { Repository } from './Repository';
import { Customer } from '../Model/Customer';
import { Product } from '../Model/Product';
import { Order } from '../Model/Order';

describe('OrderMapper', () => {
    let orderMapper: OrderMapper;

    beforeEach(async () => {
        const module = await Test.createTestingModule({
            providers: [Repository, OrderMapper],
        }).compile();

        orderMapper = module.get<OrderMapper>(OrderMapper);
    });

    it('Shoud returns array of orders', async () => {
        const orders = await orderMapper.getOrdersByDate({ date: '2019-08-08' });

        expect(Array.isArray(orders)).toBeTruthy()
        expect(orders.length).toBe(3);
        expect(orders[0] instanceof Order).toBeTruthy();
        expect(orders[0].customer instanceof Customer).toBeTruthy();
        expect(orders[0].products[0] instanceof Product).toBeTruthy();
    });
});