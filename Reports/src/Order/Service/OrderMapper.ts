import { Injectable, Inject } from '@nestjs/common';
import { Repository } from './Repository';
import { Order } from '../Model/Order';
import { plainToClass } from 'class-transformer';
import * as moment from 'moment';
import { Customer } from '../Model/Customer';
import { Product } from '../Model/Product';

@Injectable()
export class OrderMapper {
  @Inject() repository: Repository;

  async getOrdersByDate({ date }: {date?: string}): Promise<Order[]> {
      let [customers, products, orders] = await Promise.all([
        this.repository.fetchCustomers(),
        this.repository.fetchProducts(),
        this.repository.fetchOrders()
      ])

      if(date) {
        orders = orders.filter(order => moment(order.createdAt).isSame(moment(date)))
      }

      return orders.map(order => {
          return plainToClass(Order, {
            ...order,
            customer: plainToClass(Customer, customers.find(customer => customer.id === order.customer)),
            products: order.products.map(orderProduct => plainToClass(Product, products.find(product => product.id == orderProduct)))
          })
      })

  }
}
