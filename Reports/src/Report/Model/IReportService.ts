import { IBestBuyers, IBestSellers } from "./IReports";

export interface IReportService {

    getBestBuyersByDate(date: string): Promise<IBestBuyers[]>

    getBestSellersByDate(date: string): Promise<IBestSellers[]>
}
