import { PipeTransform, Injectable, ArgumentMetadata, BadRequestException } from '@nestjs/common';
import * as moment from 'moment';

@Injectable()
export class ValidationParamsDatePipe implements PipeTransform<any> {
  async transform(value: any) {
    if (!(typeof value === "string" && moment(value, 'YYYY-MM-DD', true).isValid())) {
      throw new BadRequestException('You must enter a date in the format YYYY-MM-DD')
    }

    return value;
  }
}