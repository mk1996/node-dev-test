import { Controller, Get, Param, UsePipes } from '@nestjs/common';
import { ReportService } from '../Service/ReportService';
import { ValidationParamsDatePipe } from '../Pipe/ValidationParamsDate.pipe';

@Controller()
export class ReportController {

  constructor(private readonly reportService: ReportService) {}

  @Get("/report/products/:date")
  @UsePipes(ValidationParamsDatePipe)
  bestSellers(@Param("date") date: string) {
    return this.reportService.getBestSellersByDate(date)
  }

  @Get("/report/customer/:date")
  @UsePipes(ValidationParamsDatePipe)
  bestBuyers(@Param("date") date: string) {
    return this.reportService.getBestBuyersByDate(date)
  }
 
}

