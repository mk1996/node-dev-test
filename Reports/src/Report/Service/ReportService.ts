import { Injectable, Inject } from '@nestjs/common';
import { IBestBuyers, IBestSellers } from '../Model/IReports';
import { Order } from '../../Order/Model/Order';
import { Product } from '../../Order/Model/Product';
import { IReportService } from '../Model/IReportService';
import { OrderMapper } from '../../Order/Service/OrderMapper';


@Injectable()
export class ReportService implements IReportService { 
    @Inject() private readonly orderMapper: OrderMapper;
     
    public async getBestBuyersByDate(date: string): Promise<IBestBuyers[]> {
        const orders = await this.orderMapper.getOrdersByDate({date: date})

        const buyers = Object.values(orders.reduce(this.getTotalPriceByCustomer, {}) )

        return buyers.sort(this.sortByTotalPrice) as IBestBuyers[]
    }


    private getTotalPriceByCustomer(prev: {[key: number]: IBestBuyers}, order: Order): {[key: number]: IBestBuyers} {
        if(!prev[order.customer.id]) {
            prev[order.customer.id] = {
                customerName: order.customer.fullName,
                totalPrice: 0
            }
        }

        return {
            ...prev,
            [order.customer.id]: {
                ...prev[order.customer.id],
                totalPrice: +(prev[order.customer.id].totalPrice + order.totalPrice).toFixed(12)
            }
        }
    }

    private sortByTotalPrice(a: IBestBuyers, b: IBestBuyers): number {
        return b.totalPrice - a.totalPrice;
    }

    private sortByQuantity(a: IBestSellers, b: IBestSellers): number {
        return b.quantity - a.quantity;
    }

    public async getBestSellersByDate(date: string): Promise<IBestSellers[]> {
        const orders = await this.orderMapper.getOrdersByDate({date: date})

        const sellers = orders
            .map(order => order.products)
            .reduce((prev, product) => [...prev, ...product], [])
            .reduce((prev, product) => this.reduceProducts(prev, product), {})

        return Object.values(sellers).sort(this.sortByQuantity) as IBestSellers[]
    }

    private reduceProducts(prev: {[key: number]: IBestSellers}, product: Product): {[key: number]: IBestSellers} {
        if(!prev[product.id]) {
            prev[product.id] = {
                quantity: 0,
                totalPrice: 0,
                productName: product.name,
            }
        }

        return {
            ...prev,
            [product.id]: {
                ...prev[product.id],
                quantity: prev[product.id].quantity + 1,
                totalPrice: +(prev[product.id].totalPrice + product.price).toFixed(12)
            }
        }
    }
}