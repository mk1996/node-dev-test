export class BSort {

    private constructor() {}
    
    static sort(items: number[]): number[] {
        return BSort.countingSort(items, 0, items.length-1)
    }

    private static countingSort(items: number[], min: number, max: number): number[] {
        let i = min
        let j = 0
        let len = items.length
        let count = [];

        for (i; i <= max; i++) {
            count[i] = 0;
        }

        for (i = 0; i < len; i++) {
            count[items[i]] += 1;
        }

        for (i = min; i <= max; i++) {
            while (count[i] > 0) {
                items[j] = i;
                j++;
                count[i]--;
            }
        }
        return items;
    }
}