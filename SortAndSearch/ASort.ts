export class ASort {

    private constructor() {}

    static sort(items: number[]): number[] {
        return ASort.quickSort(items, 0, items.length-1)
    }

    private static quickSort(array: number[], left: number, right: number): number[] {
        if (array.length > 1) {
            const index = ASort.partition(array, left, right); 
            if (left < index - 1) { 
                ASort.quickSort(array, left, index - 1);
            }
            if (index < right) {
                ASort.quickSort(array, index, right);
            }
        }
        return array;
    }

    private static partition(items, left, right) {
        const pivot = items[Math.floor((right + left) / 2)]
        let i = left 
        let j = right

        while (i <= j) {
            while (items[i] < pivot) {
                i++;
            }
            while (items[j] > pivot) {
                j--;
            }
            if (i <= j) {
                [items[i], items[j]] = [items[j], items[i]];
                i++;
                j--;
            }
        }
        return i;
    }

}