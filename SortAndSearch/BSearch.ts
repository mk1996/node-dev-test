export class BSearch {
    private static _instance: BSearch;
    private _operations: number = 0;

    private constructor() { }

    operations(): number {
        return this._operations;
    }

    find<T extends String | Number>(array: T[], elementToFind: T): number {
        return this.binarySearch(array, elementToFind)
    }

    static getInstance(): BSearch {
        if(!BSearch._instance) {
            BSearch._instance = new BSearch()
        } 

        return BSearch._instance
    }

    private binarySearch<T>(array: T[], element: T): number {
        this._operations++;
        let minIndex = 0;
        let maxIndex = array.length - 1;
        while (minIndex <= maxIndex) {
            const midIndex = Math.floor((minIndex + maxIndex) / 2);
            if (array[midIndex] == element) {
                return midIndex;
            } else if (array[midIndex] < element) {
                minIndex = midIndex + 1;
            } else {
                maxIndex = midIndex - 1;
            }
        } return -1;
    }
}